<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CaTime */

$this->title = 'Criar Time';
$this->params['breadcrumbs'][] = ['label' => 'Ca Times', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ca-time-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
