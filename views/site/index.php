<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>AC Sports!</h1>

        <p class="lead">Treinos, jogos, campeonatos e muitos mais!</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Bem-vindo(a) ao AC Sports!</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Objetivo</h2>

                <p>Nosso programa tem como objetivo auxiliar alunos e professores 
                no quesito esporte dentro do Ambiete escolar do IFNMG.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Criadores</h2>

                <p>Administradores: Clebson Santos e André Martins.<br>
                Turma: 2º Informática "A"</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Modalidades</h2>

                <p>Basquete (Masculino)<br> 
                Futsal (Masculino e Feminino)<br>
                Handebol (Masculino e Feminino)<br>
                Volei (Masculino e Feminino)</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
