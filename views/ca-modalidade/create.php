<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CaModalidade */

$this->title = 'Criar Modalidade';
$this->params['breadcrumbs'][] = ['label' => 'Ca Modalidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ca-modalidade-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
