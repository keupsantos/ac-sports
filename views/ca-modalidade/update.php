<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CaModalidade */

$this->title = 'Atualizar Modalidade: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ca Modalidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ca-modalidade-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
