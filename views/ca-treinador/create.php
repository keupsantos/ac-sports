<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CaTreinador */

$this->title = 'Criar Treinador';
$this->params['breadcrumbs'][] = ['label' => 'Ca Treinadors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ca-treinador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
