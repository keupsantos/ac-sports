<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CaTreinador */

$this->title = 'Atualizar Treinador: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ca Treinadors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ca-treinador-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
