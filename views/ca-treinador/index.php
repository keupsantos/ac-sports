<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CaTreinadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Treinadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ca-treinador-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar Treinador', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nome',
            'login',
            'senha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
