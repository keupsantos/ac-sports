<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CaFrequencia */

$this->title = 'Criar Frequencia';
$this->params['breadcrumbs'][] = ['label' => 'Ca Frequencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ca-frequencia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
