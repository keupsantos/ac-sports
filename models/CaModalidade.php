<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ca_modalidade".
 *
 * @property int $id
 * @property string $nome
 *
 * @property CaTime[] $caTimes
 */
class CaModalidade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ca_modalidade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaTimes()
    {
        return $this->hasMany(CaTime::className(), ['modalidade_id' => 'id']);
    }
}
