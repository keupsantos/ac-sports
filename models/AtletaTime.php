<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "atleta_time".
 *
 * @property int $atleta_id
 * @property int $time_id
 * @property string $inserido
 * @property string $posicao
 *
 * @property CaAtleta $atleta
 * @property CaTime $time
 */
class AtletaTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atleta_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['atleta_id', 'time_id'], 'required'],
            [['atleta_id', 'time_id'], 'integer'],
            [['inserido'], 'safe'],
            [['posicao'], 'string', 'max' => 20],
            [['atleta_id', 'time_id'], 'unique', 'targetAttribute' => ['atleta_id', 'time_id']],
            [['atleta_id'], 'exist', 'skipOnError' => true, 'targetClass' => CaAtleta::className(), 'targetAttribute' => ['atleta_id' => 'id']],
            [['time_id'], 'exist', 'skipOnError' => true, 'targetClass' => CaTime::className(), 'targetAttribute' => ['time_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'atleta_id' => 'Atleta ID',
            'time_id' => 'Time ID',
            'inserido' => 'Inserido',
            'posicao' => 'Posicao',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtleta()
    {
        return $this->hasOne(CaAtleta::className(), ['id' => 'atleta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTime()
    {
        return $this->hasOne(CaTime::className(), ['id' => 'time_id']);
    }
}
