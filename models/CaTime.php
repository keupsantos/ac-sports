<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ca_time".
 *
 * @property int $id
 * @property string $nome
 * @property int $modalidade_id
 * @property int $treinador_id
 *
 * @property AtletaTime[] $atletaTimes
 * @property CaAtleta[] $atletas
 * @property CaFrequencia[] $caFrequencias
 * @property CaModalidade $modalidade
 * @property CaTreinador $treinador
 */
class CaTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ca_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['modalidade_id', 'treinador_id'], 'integer'],
            [['nome'], 'string', 'max' => 200],
            [['modalidade_id'], 'exist', 'skipOnError' => true, 'targetClass' => CaModalidade::className(), 'targetAttribute' => ['modalidade_id' => 'id']],
            [['treinador_id'], 'exist', 'skipOnError' => true, 'targetClass' => CaTreinador::className(), 'targetAttribute' => ['treinador_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'modalidade_id' => 'Modalidade ID',
            'treinador_id' => 'Treinador ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtletaTimes()
    {
        return $this->hasMany(AtletaTime::className(), ['time_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtletas()
    {
        return $this->hasMany(CaAtleta::className(), ['id' => 'atleta_id'])->viaTable('atleta_time', ['time_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaFrequencias()
    {
        return $this->hasMany(CaFrequencia::className(), ['time_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModalidade()
    {
        return $this->hasOne(CaModalidade::className(), ['id' => 'modalidade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTreinador()
    {
        return $this->hasOne(CaTreinador::className(), ['id' => 'treinador_id']);
    }
}
