<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ca_atleta".
 *
 * @property int $id
 * @property string $sexo
 * @property string $nome
 * @property string $turma
 *
 * @property AtletaTime[] $atletaTimes
 * @property CaTime[] $times
 * @property CaFrequencia[] $caFrequencias
 */
class CaAtleta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ca_atleta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sexo', 'nome', 'turma'], 'required'],
            [['sexo'], 'string', 'max' => 2],
            [['nome'], 'string', 'max' => 200],
            [['turma'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sexo' => 'Sexo',
            'nome' => 'Nome',
            'turma' => 'Turma',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtletaTimes()
    {
        return $this->hasMany(AtletaTime::className(), ['atleta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimes()
    {
        return $this->hasMany(CaTime::className(), ['id' => 'time_id'])->viaTable('atleta_time', ['atleta_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaFrequencias()
    {
        return $this->hasMany(CaFrequencia::className(), ['atleta_id' => 'id']);
    }
}
