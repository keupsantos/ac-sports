<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CaFrequencia;

/**
 * CaFrequenciaSearch represents the model behind the search form of `app\models\CaFrequencia`.
 */
class CaFrequenciaSearch extends CaFrequencia
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'atleta_id', 'time_id'], 'integer'],
            [['data', 'presenca'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CaFrequencia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data' => $this->data,
            'atleta_id' => $this->atleta_id,
            'time_id' => $this->time_id,
        ]);

        $query->andFilterWhere(['like', 'presenca', $this->presenca]);

        return $dataProvider;
    }
}
