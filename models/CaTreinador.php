<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ca_treinador".
 *
 * @property int $id
 * @property string $nome
 * @property string $login
 * @property string $senha
 *
 * @property CaTime[] $caTimes
 */
class CaTreinador extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ca_treinador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 200],
            [['login'], 'string', 'max' => 30],
            [['senha'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'login' => 'Login',
            'senha' => 'Senha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaTimes()
    {
        return $this->hasMany(CaTime::className(), ['treinador_id' => 'id']);
    }
}
