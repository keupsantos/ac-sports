<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ca_frequencia".
 *
 * @property int $id
 * @property string $data
 * @property string $presenca
 * @property int $atleta_id
 * @property int $time_id
 *
 * @property CaAtleta $atleta
 * @property CaTime $time
 */
class CaFrequencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ca_frequencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['atleta_id', 'time_id'], 'integer'],
            [['presenca'], 'string', 'max' => 2],
            [['atleta_id'], 'exist', 'skipOnError' => true, 'targetClass' => CaAtleta::className(), 'targetAttribute' => ['atleta_id' => 'id']],
            [['time_id'], 'exist', 'skipOnError' => true, 'targetClass' => CaTime::className(), 'targetAttribute' => ['time_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'presenca' => 'Presenca',
            'atleta_id' => 'Atleta ID',
            'time_id' => 'Time ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtleta()
    {
        return $this->hasOne(CaAtleta::className(), ['id' => 'atleta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTime()
    {
        return $this->hasOne(CaTime::className(), ['id' => 'time_id']);
    }
}
